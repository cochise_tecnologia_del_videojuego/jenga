using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceRoll : MonoBehaviour
{
    Rigidbody m_Rigidbody;
    public GameObject dice;

    void Start()
    {
        //Fetch the Rigidbody from the GameObject with this script attached
        m_Rigidbody = GetComponent<Rigidbody>();
        dice.transform.position = new Vector3(0, 30, 15);
        dice.transform.Rotate(Random.Range(0, 360), Random.Range(0, 360), Random.Range(0, 360));
        m_Rigidbody.AddTorque(Random.Range(5000, 10000), Random.Range(5000, 10000), Random.Range(5000, 10000));
        

    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Destroy(dice);
        }
    }
    
}
